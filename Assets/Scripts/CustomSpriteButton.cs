using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    public class CustomSpriteButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public CustomButton button;
        public GameObject chooseYourSymbolUI;
        public CellZone cellZone1;
        public CellZone cellZone2;

        public Vector2 Size => button.GetComponent<RectTransform>().sizeDelta;
        public TextMeshPro textMeshPro;


        private void OnEnable()
        {
            string buttonText = textMeshPro.text;

            if (buttonText == "Star")
            {
                button.Select();
            }
        }

        public void Click()
        {
            textMeshPro.color = Color.white;
            string buttonText = textMeshPro.text;
            Debug.Log("Click");
            if(buttonText == "Star")
            {
                cellZone1.starSelected = true;
                cellZone2.starSelected = true;
            }
            else if(buttonText == "Moon")
            {
                cellZone1.moonSelected = true;
                cellZone2.moonSelected = true;
            }
            else if(buttonText == "Sun")
            {
                cellZone1.sunSelected = true;
                cellZone2.sunSelected = true;
            }
            else if(buttonText == "Cloud")
            {
                cellZone1.cloudSelected = true;
                cellZone2.cloudSelected = true;
            }
            chooseYourSymbolUI.SetActive(false);
        }

        private void Start()
        {
            // Add the Click method as a listener for the button's onClick event
            button.onClick.AddListener(Click);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            button.OnPointerEnter(eventData);
            EventSystem.current.SetSelectedGameObject(null);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            button.Select();
        }
    }
}