using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using TMPro;
using UnityEditor.U2D.Common;
using System.Diagnostics;

public class PlayerSymbol : NetworkBehaviour
{
    public enum Symbol
    {
        Star,   // 0
        Moon,   // 1
        Sun,    // 2
        Cloud   // 3
    }

    // The player's symbol
    public static Symbol playerSymbol;
    private NetworkVariable<Symbol> networkPlayerSymbol = new NetworkVariable<Symbol>();
    public NetworkVariable<bool> isImposter = new NetworkVariable<bool>();

    private TextMeshPro imposterText;

    public override void OnNetworkSpawn()
    {
        networkPlayerSymbol.OnValueChanged += (Symbol oldValue, Symbol newValue) => 
        {
            playerSymbol = newValue;
        };

        isImposter.OnValueChanged += (bool oldValue, bool newValue) =>
        {
            UpdateImposterText();
        };

        if (IsServer)
        {
            AssignRandomSymbol();
        }
    }

    public void AssignRandomSymbol()
    {
        if (!IsServer) return;
        
        Random.InitState(System.DateTime.Now.Millisecond);
        // Get a random number
        int randomNumber = Random.Range(0, System.Enum.GetValues(typeof(Symbol)).Length);

        // Assign the corresponding symbol to the player
        networkPlayerSymbol.Value = (Symbol)randomNumber;
        // Static value will be updated via OnValueChanged

        if(networkPlayerSymbol.Value == Symbol.Star)
        {
            UnityEngine.Debug.Log("Player's symbol is Star");
        }
        else if(networkPlayerSymbol.Value == Symbol.Moon)
        {
            UnityEngine.Debug.Log("Player's symbol is Moon");
        }
        else if(networkPlayerSymbol.Value == Symbol.Sun)
        {
            UnityEngine.Debug.Log("Player's symbol is Sun");
        }
        else if(networkPlayerSymbol.Value == Symbol.Cloud)
        {
            UnityEngine.Debug.Log("Player's symbol is Cloud");
        }
    }

    public void UpdateImposterText()
    {
        if(IsOwner)
        {
            if(!imposterText)
            {
                imposterText = GameObject.Find("ImposterText").GetComponent<TextMeshPro>();
            }
            if(isImposter.Value)
            {
                imposterText.text = "You are the Imposter";
            }
            else
            {
                imposterText.text = "You are NOT the Imposter";
            }
        }
    }

    public void ShowSymbolToPlayer()
    {
        SymbolManager.SetSymbol((int)playerSymbol);
        SymbolManager.DisplayCurrentSymbol();
    }
}
