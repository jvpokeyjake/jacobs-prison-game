using TMPro;
using Unity.Netcode;
using UnityEngine;
using LobbyRelaySample.UI;
using Unity.Collections; // Import this for FixedString

public class UsernameUpdate : NetworkBehaviour
{
    [SerializeField] private TextMeshProUGUI userNameText;

    // Use FixedString128Bytes for the NetworkVariable
    public NetworkVariable<FixedString128Bytes> playerName = new NetworkVariable<FixedString128Bytes>(
        writePerm: NetworkVariableWritePermission.Server // Only the server can write to it
    );

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();

        // Subscribe to changes in the playerName NetworkVariable
        playerName.OnValueChanged += OnPlayerNameChanged;

        // Update the username if this is the owner
        if (IsOwner)
        {
            UpdateUsernameServerRpc(UserNameUI.PlayerUserName);
        }

        // Initialize the UI text with the current value
        if (userNameText != null)
        {
            userNameText.text = playerName.Value.ToString();
        }
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();

        // Unsubscribe from the NetworkVariable event
        playerName.OnValueChanged -= OnPlayerNameChanged;
    }

    private void OnPlayerNameChanged(FixedString128Bytes oldName, FixedString128Bytes newName)
    {
        // Update the UI text when the playerName changes
        if (userNameText != null)
        {
            userNameText.text = newName.ToString();
        }
    }

    [ServerRpc]
    private void UpdateUsernameServerRpc(FixedString128Bytes newName)
    {
        playerName.Value = newName; // Update the NetworkVariable on the server
    }
}