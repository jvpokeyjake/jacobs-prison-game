using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
    public class StartMenuSelection : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public CustomButton button;
        public TextMeshProUGUI textMeshPro;


        private void OnEnable()
        {
            string buttonText = textMeshPro.text;

            if (buttonText == "Start")
            {
                button.Select();
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            button.OnPointerEnter(eventData);
            EventSystem.current.SetSelectedGameObject(null);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            button.Select();
        }
    }
}