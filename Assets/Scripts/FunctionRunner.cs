using UnityEngine;
using UnityEngine.Events;

public class FunctionRunner : MonoBehaviour
{
    public UnityEvent myEvent;

    public void Run()
    {
        if (myEvent != null)
        {
            myEvent.Invoke();
        }
    }
}