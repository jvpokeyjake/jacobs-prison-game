using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;

public class NPCSymbol : NetworkBehaviour
{
    public enum Symbol
    {
        Star,   // 0
        Moon,   // 1
        Sun,    // 2
        Cloud   // 3
    }

    // The NPC's symbol
    [HideInInspector] public Symbol npcSymbol;
    private NetworkVariable<Symbol> networkNpcSymbol = new NetworkVariable<Symbol>();
    private NetworkVariable<float> guessSuccessRate = new NetworkVariable<float>(0.75f);
    public NetworkVariable<bool> isImposter = new NetworkVariable<bool>();

    public override void OnNetworkSpawn()
    {
        networkNpcSymbol.OnValueChanged += (Symbol oldValue, Symbol newValue) => 
        {
            npcSymbol = newValue;
        };

        if (IsServer)
        {
            AssignRandomSymbol();
        }
    }

    public void AssignRandomSymbol()
    {
        if (!IsServer) return;

        Random.InitState(System.DateTime.Now.Millisecond);
        // Get a random number
        int randomNumber = Random.Range(0, System.Enum.GetValues(typeof(Symbol)).Length);

        // Assign the corresponding symbol to the NPC
        networkNpcSymbol.Value = (Symbol)randomNumber;
        // Public field will be updated via OnValueChanged
        guessSuccessRate.Value = 0.75f;

        if(networkNpcSymbol.Value == Symbol.Star)
        {
            Debug.Log("NPC's symbol is Star");
        }
        else if(networkNpcSymbol.Value == Symbol.Moon)
        {
            Debug.Log("NPC's symbol is Moon");
        }
        else if(networkNpcSymbol.Value == Symbol.Sun)
        {
            Debug.Log("NPC's symbol is Sun");
        }
        else if(networkNpcSymbol.Value == Symbol.Cloud)
        {
            Debug.Log("NPC's symbol is Cloud");
        }
    }

    public Symbol GuessSymbol()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        // Generate a random float between 0 and 1
        float randomFloat = Random.Range(0f, 1f);

        // If the random float is less than the guess success rate, the NPC guesses their symbol correctly
        if (randomFloat < guessSuccessRate.Value)
        {
            return npcSymbol;
        }
        // Otherwise, the NPC guesses a random symbol
        else
        {
            return (Symbol)Random.Range(0, System.Enum.GetValues(typeof(Symbol)).Length);
        }
    }

    public void IncreaseGuessSuccessRate()
    {
        if (!IsServer) return;
        // Increase the guess success rate by 20%, making sure it doesn't exceed 100%
        guessSuccessRate.Value = Mathf.Min(guessSuccessRate.Value + 0.2f, 1f);
        Debug.Log("NPC guess success rate: " + guessSuccessRate.Value);
    }

    public void DecreaseGuessSuccessRate()
    {
        if (!IsServer) return;
        // Decrease the guess success rate by 20%, making sure it doesn't drop below 0%
        guessSuccessRate.Value = Mathf.Max(guessSuccessRate.Value - 0.2f, 0f);
        Debug.Log("NPC guess success rate: " + guessSuccessRate.Value);
    }

    public void TellPlayerSymbol()
    {
        // If the NPC is an imposter, tell the player a random symbol
        if (isImposter.Value)
        {
            // Get a random number
            int randomNumber = Random.Range(0, System.Enum.GetValues(typeof(Symbol)).Length);

            // Tell the player the symbol
            SymbolManager.SetSymbol(randomNumber);
            SymbolManager.DisplayCurrentSymbol();
        }
        else
        {
            // Tell the player the correct symbol
            SymbolManager.SetSymbol((int)PlayerSymbol.playerSymbol);
            SymbolManager.DisplayCurrentSymbol();
        }
    }

    public void ShowSymbolToPlayer()
    {
        SymbolManager.SetSymbol((int)npcSymbol);
        SymbolManager.DisplayCurrentSymbol();
    }

    public void GetToldStarSymbol()
    {
        Debug.Log("NPC was told Star symbol");
        if(npcSymbol == Symbol.Star)
        {
            IncreaseGuessSuccessRate();
        }
        else
        {
            DecreaseGuessSuccessRate();
        }
    }

    public void GetToldMoonSymbol()
    {
        Debug.Log("NPC was told Moon symbol");
        if (npcSymbol == Symbol.Moon)
        {
            IncreaseGuessSuccessRate();
        }
        else
        {
            DecreaseGuessSuccessRate();
        }
    }

    public void GetToldSunSymbol()
    {
        Debug.Log("NPC was told Sun symbol");
        if (npcSymbol == Symbol.Sun)
        {
            IncreaseGuessSuccessRate();
        }
        else
        {
            DecreaseGuessSuccessRate();
        }
    }

    public void GetToldCloudSymbol()
    {
        Debug.Log("NPC was told Cloud symbol");
        if (npcSymbol == Symbol.Cloud)
        {
            IncreaseGuessSuccessRate();
        }
        else
        {
            DecreaseGuessSuccessRate();
        }
    }
}