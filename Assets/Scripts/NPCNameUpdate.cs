using TMPro;
using Unity.Netcode;
using UnityEngine;
using LobbyRelaySample.UI;
using Unity.Collections;

public class NPCNameUpdate : NetworkBehaviour
{
    [SerializeField] private TextMeshProUGUI npcNameText;

    public static int NPCNumber = 1;

    void Start()
    {
        npcNameText.text = "NPC "+NPCNumber;
        NPCNumber++;
    }
}
