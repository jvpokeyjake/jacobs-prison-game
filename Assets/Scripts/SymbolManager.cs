using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SymbolManager : MonoBehaviour
{
    public enum Symbol
    {
        Star,
        Moon,
        Sun,
        Cloud
    }

    public static Symbol currentSymbol;

    public static Image starImage;
    public static Image moonImage;
    public static Image sunImage;
    public static Image cloudImage;

    void Awake()
    {
        starImage = transform.Find("Canvas/Star").GetComponent<Image>();
        moonImage = transform.Find("Canvas/Moon").GetComponent<Image>();
        sunImage = transform.Find("Canvas/Sun").GetComponent<Image>();
        cloudImage = transform.Find("Canvas/Cloud").GetComponent<Image>();
    }

    void Start()
    {
        // Set the initial symbol
        SetSymbol((int)Symbol.Star);

        // Disable the current symbol
        DisableCurrentSymbol();
    }

    void Update()
    {
        // Check for spacebar press
        if (Input.GetKeyDown(KeyCode.Space))
        {
            // Disable the current symbol
            DisableCurrentSymbol();
        }
    }

    public static void SetSymbol(int symbol)
    {
        // Disable the current symbol
        DisableCurrentSymbol();

        // Set the new symbol
        currentSymbol = (Symbol)symbol;
    }

    public static void DisplayCurrentSymbol()
    {
        // Enable the current symbol
        EnableCurrentSymbol();
    }

    static void DisableCurrentSymbol()
    {
        switch (currentSymbol)
        {
            case Symbol.Star:
                starImage.enabled = false;
                break;
            case Symbol.Moon:
                moonImage.enabled = false;
                break;
            case Symbol.Sun:
                sunImage.enabled = false;
                break;
            case Symbol.Cloud:
                cloudImage.enabled = false;
                break;
        }
    }

    static void EnableCurrentSymbol()
    {
        switch (currentSymbol)
        {
            case Symbol.Star:
                starImage.enabled = true;
                break;
            case Symbol.Moon:
                moonImage.enabled = true;
                break;
            case Symbol.Sun:
                sunImage.enabled = true;
                break;
            case Symbol.Cloud:
                cloudImage.enabled = true;
                break;
        }
    }
}