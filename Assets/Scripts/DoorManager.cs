using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class DoorManager : MonoBehaviour
{
    public Tilemap foregroundTileMap;
    public Tilemap doorTileMap;
    public Tilemap hiddenDoorTileMap;
    public Tilemap doorOpenBackTilemap;
    public Tilemap doorOpenFrontTilemap;
    public bool isLocked = false;

    private Dictionary<Vector3Int, (TileBase tile, bool wasSpriteCollider)> originalForegroundTiles = new Dictionary<Vector3Int, (TileBase, bool)>();
    private Dictionary<Vector3Int, TileBase> originalDoorTiles = new Dictionary<Vector3Int, TileBase>();
    private Dictionary<Vector3Int, TileBase> originalHiddenDoorTiles = new Dictionary<Vector3Int, TileBase>();
    private Dictionary<Vector3Int, TileBase> originalDoorOpenBackTiles = new Dictionary<Vector3Int, TileBase>();
    private Dictionary<Vector3Int, TileBase> originalDoorOpenFrontTiles = new Dictionary<Vector3Int, TileBase>();
    private TilemapCollider2D doorTilemapCollider;
    private TilemapCollider2D hiddenDoorTilemapCollider;

    private static int currentCollisionCount = 0;

    void Start()
    {
        foreach (var pos in doorTileMap.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            TileBase tile = doorTileMap.GetTile(localPlace);
            if (tile != null)
            {
                Tile t = tile as Tile;
                if (t != null && t.colliderType != Tile.ColliderType.None)
                {
                    GameObject colliderObject = new GameObject("Collider");
                    colliderObject.transform.parent = doorTileMap.gameObject.transform;
                    colliderObject.transform.position = doorTileMap.GetCellCenterWorld(localPlace);
                    BoxCollider2D boxCollider = colliderObject.AddComponent<BoxCollider2D>();
                    boxCollider.size = new Vector2(1.1f, 1.1f);
                    boxCollider.isTrigger = true;
                    boxCollider.offset = new Vector2(0, 0);
                }
            }
        }
        doorTilemapCollider = doorTileMap.GetComponent<TilemapCollider2D>();
        hiddenDoorTilemapCollider = hiddenDoorTileMap.GetComponent<TilemapCollider2D>();

        foreach (var pos in hiddenDoorTileMap.cellBounds.allPositionsWithin)
        {
            Vector3Int localPlace = new Vector3Int(pos.x, pos.y, pos.z);
            TileBase tile = hiddenDoorTileMap.GetTile(localPlace);
            if (tile != null)
            {
                Tile t = tile as Tile;
                if (t != null && t.colliderType != Tile.ColliderType.None)
                {
                    GameObject colliderObject = new GameObject("Hidden Collider");
                    colliderObject.transform.parent = hiddenDoorTileMap.gameObject.transform;
                    colliderObject.transform.position = hiddenDoorTileMap.GetCellCenterWorld(localPlace);
                    BoxCollider2D boxCollider = colliderObject.AddComponent<BoxCollider2D>();
                    boxCollider.size = new Vector2(1.2f, 1.2f);
                    boxCollider.isTrigger = true;
                    boxCollider.offset = new Vector2(0, 0);
                }
            }
        }
        TilemapCollider2D hiddenTilemapCollider = hiddenDoorTileMap.GetComponent<TilemapCollider2D>();
        hiddenTilemapCollider.enabled = false;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        // if (isLocked)
        // {
        //     Debug.Log("DoorManager: OnTriggerEnter2D - Door Locked");
        //     return;
        // }
        Debug.Log("DoorManager: OnTriggerEnter2D - Door Unlocked");
        doorTilemapCollider.enabled = false;
        hiddenDoorTilemapCollider.enabled = false;
        currentCollisionCount++;
        Vector3 hitPosition = collision.transform.position;
        Vector3Int cellPosition = doorTileMap.WorldToCell(hitPosition);
        Vector3Int hiddenCellPosition = hiddenDoorTileMap.WorldToCell(hitPosition);

        for (int i = -3; i <= 3; i++)
        {
            for (int j = -2; j <= 2; j++)
            {
                Vector3Int currentCellPosition = new Vector3Int(cellPosition.x + j, cellPosition.y + i, cellPosition.z);
                Vector3Int currentHiddenCellPosition = new Vector3Int(hiddenCellPosition.x + j, hiddenCellPosition.y + i, hiddenCellPosition.z);
                TileBase doorTile = doorTileMap.GetTile(currentCellPosition);
                TileBase hiddenDoorTile = hiddenDoorTileMap.GetTile(currentHiddenCellPosition);
                TileBase doorOpenBackTile = doorOpenBackTilemap.GetTile(currentCellPosition);
                TileBase doorOpenFrontTile = doorOpenFrontTilemap.GetTile(currentCellPosition);

                // If either doorTile or hiddenDoorTile is not null, hide both doors
                if (doorTile != null || hiddenDoorTile != null || doorOpenBackTile != null || doorOpenFrontTile != null)
                {
                    // Hide doorTile
                    if (doorTile != null)
                    {
                        originalDoorTiles[currentCellPosition] = doorTile;
                        doorTileMap.SetTileFlags(currentCellPosition, TileFlags.None);
                        doorTileMap.SetColor(currentCellPosition, new Color(1, 1, 1, 0));
                    }

                    // Hide hiddenDoorTile
                    if (hiddenDoorTile != null)
                    {
                        originalHiddenDoorTiles[currentHiddenCellPosition] = hiddenDoorTile;
                        hiddenDoorTileMap.SetTileFlags(currentHiddenCellPosition, TileFlags.None);
                        hiddenDoorTileMap.SetColor(currentHiddenCellPosition, new Color(1, 1, 1, 0));
                    }

                    // Hide foreground tiles
                    TileBase foregroundTile = foregroundTileMap.GetTile(currentCellPosition);
                    if (foregroundTile != null)
                    {
                        originalForegroundTiles[currentCellPosition] = (foregroundTile, ((Tile)foregroundTile).colliderType == Tile.ColliderType.Sprite);
                        foregroundTileMap.SetTileFlags(currentCellPosition, TileFlags.None);
                        foregroundTileMap.SetColor(currentCellPosition, new Color(1, 1, 1, 0));
                        foregroundTileMap.SetColliderType(currentCellPosition, Tile.ColliderType.None);
                    }

                    // Set doorOpenBackTilemap alpha to 1
                    if (doorOpenBackTile != null)
                    {
                        originalDoorOpenBackTiles[currentCellPosition] = doorOpenBackTile;
                        doorOpenBackTilemap.SetTileFlags(currentCellPosition, TileFlags.None);
                        doorOpenBackTilemap.SetColor(currentCellPosition, new Color(1, 1, 1, 1));
                    }

                    // Set doorOpenFrontTilemap alpha to 1
                    if (doorOpenFrontTile != null)
                    {
                        originalDoorOpenFrontTiles[currentCellPosition] = doorOpenFrontTile;
                        doorOpenFrontTilemap.SetTileFlags(currentCellPosition, TileFlags.None);
                        doorOpenFrontTilemap.SetColor(currentCellPosition, new Color(1, 1, 1, 1));
                    }
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        Debug.Log("DoorManager: OnTriggerExit2D");
        currentCollisionCount--;
        
        if (currentCollisionCount == 0)
        {
            doorTilemapCollider.enabled = true;
            hiddenDoorTilemapCollider.enabled = true;
            foreach (KeyValuePair<Vector3Int, (TileBase tile, bool wasSpriteCollider)> originalForegroundTile in originalForegroundTiles)
            {
                foregroundTileMap.SetTile(originalForegroundTile.Key, originalForegroundTile.Value.tile);
                foregroundTileMap.SetColor(originalForegroundTile.Key, new Color(1, 1, 1, 1));
                if (originalForegroundTile.Value.wasSpriteCollider)
                {
                    foregroundTileMap.SetColliderType(originalForegroundTile.Key, Tile.ColliderType.Sprite);
                }
            }
            foreach (KeyValuePair<Vector3Int, TileBase> originalDoorTile in originalDoorTiles)
            {
                doorTileMap.SetTile(originalDoorTile.Key, originalDoorTile.Value);
                doorTileMap.SetColor(originalDoorTile.Key, new Color(1, 1, 1, 1));
            }
            foreach (KeyValuePair<Vector3Int, TileBase> originalHiddenDoorTile in originalHiddenDoorTiles)
            {
                hiddenDoorTileMap.SetTile(originalHiddenDoorTile.Key, originalHiddenDoorTile.Value);
                hiddenDoorTileMap.SetColor(originalHiddenDoorTile.Key, new Color(1, 1, 1, 1));
            }
            foreach (KeyValuePair<Vector3Int, TileBase> originalDoorOpenBackTile in originalDoorOpenBackTiles)
            {
                // Set doorOpenBackTilemap alpha to 0
                doorOpenBackTilemap.SetTile(originalDoorOpenBackTile.Key, originalDoorOpenBackTile.Value);
                doorOpenBackTilemap.SetColor(originalDoorOpenBackTile.Key, new Color(1, 1, 1, 0));
            }
            foreach (KeyValuePair<Vector3Int, TileBase> originalDoorOpenFrontTile in originalDoorOpenFrontTiles)
            {
                // Set doorOpenFrontTilemap alpha to 0
                doorOpenFrontTilemap.SetTile(originalDoorOpenFrontTile.Key, originalDoorOpenFrontTile.Value);
                doorOpenFrontTilemap.SetColor(originalDoorOpenFrontTile.Key, new Color(1, 1, 1, 0));
            }

            originalForegroundTiles.Clear();
            originalDoorTiles.Clear();
            originalHiddenDoorTiles.Clear();
        }
    }

    void Update()
    {
        if (RoundAndTime.time >= 23 * 60 || RoundAndTime.time <= 06.5 * 60)
        {
            isLocked = true;
        }
        else
        {
            isLocked = false;
        }
    }
}