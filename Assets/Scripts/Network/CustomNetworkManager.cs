using Unity.Netcode;
using System.Collections;
using UnityEngine;

public class CustomNetworkManager : NetworkManager
{
    string lobbyCode;

    public void StartGameAsHost()
    {
        StartCoroutine(StartHostWhenReady());
    }

    IEnumerator StartHostWhenReady()
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex != 1)
        {
            StartHost();
        }
        else
        {
            yield return new WaitForSeconds(1);
            StartCoroutine(StartHostWhenReady());
        }
    }
}