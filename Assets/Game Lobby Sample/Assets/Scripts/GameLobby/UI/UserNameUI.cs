using TMPro;
using UnityEngine;

namespace LobbyRelaySample.UI
{
    /// <summary>
    /// Displays the player's name and stores it in PlayerPrefs.
    /// </summary>
    public class UserNameUI : UIPanelBase
    {
        [SerializeField]
        TMP_Text m_TextField;

        private const string PlayerPrefsKey = "PlayerUserName";

        public static string PlayerUserName { get; private set; }

        public override async void Start()
        {
            base.Start();

            m_TextField.SetText(PlayerPrefs.GetString(PlayerPrefsKey));

            var localUser = await GameManager.Instance.AwaitLocalUserInitialization();
            localUser.DisplayName.onChanged += SetText;
        }

        void SetText(string text)
        {
            m_TextField.SetText(text);
            PlayerPrefs.SetString(PlayerPrefsKey, text);
            PlayerUserName = text;
            PlayerPrefs.Save();
        }
    }
}