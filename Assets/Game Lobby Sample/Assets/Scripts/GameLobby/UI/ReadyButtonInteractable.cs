using UnityEngine.UI;
using UnityEngine;

namespace LobbyRelaySample.UI
{
    public class ReadyButtonInteractable : MonoBehaviour
    {
        [SerializeField] GameManager gameManager;
        [SerializeField] Button readyButton;
        void Update()
        {
            if(gameManager.m_LocalUser.IsHost.Value)
            {
                readyButton.interactable = true;
            }
            else
            {
                readyButton.interactable = false;
            }
        }
    }
}