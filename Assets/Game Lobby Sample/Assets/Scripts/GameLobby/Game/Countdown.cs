﻿using System;
using UnityEngine;

namespace LobbyRelaySample
{
    /// <summary>
    /// Runs the countdown to the in-game state. While the start of the countdown is synced via Relay, the countdown itself is handled locally,
    /// since precise timing isn't necessary.
    /// </summary>
    [RequireComponent(typeof(UI.CountdownUI))]
    public class Countdown : MonoBehaviour
    {
        CallbackValue<float> TimeLeft = new CallbackValue<float>();

        [SerializeField] private GameManager gameManager;
        [SerializeField] GameObject readyButton;
        [SerializeField] GameObject hostStartingUI;
        private UI.CountdownUI m_ui;
        private const int k_countdownTime = 4;
        private bool countdownStarted = false;

        public void OnEnable()
        {
            if (m_ui == null)
                m_ui = GetComponent<UI.CountdownUI>();
            TimeLeft.onChanged += m_ui.OnTimeChanged;
            TimeLeft.Value = -1;
        }

        public void StartCountDown()
        {
            TimeLeft.Value = k_countdownTime;
        }

        public void CancelCountDown()
        {
            TimeLeft.Value = -1;
        }

        public void Update()
        {
            if (TimeLeft.Value < 0)
                return;
            if(!gameManager.m_LocalUser.IsHost.Value)
            {
                readyButton.SetActive(false);
                hostStartingUI.SetActive(true);
            }
            TimeLeft.Value -= Time.deltaTime;
            if (TimeLeft.Value < 0)
            //check from the non-host side if the host is still ready
                GameManager.Instance.FinishedCountDown();
        }
    }
}