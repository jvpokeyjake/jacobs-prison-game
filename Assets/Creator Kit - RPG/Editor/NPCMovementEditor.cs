#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

[CustomEditor(typeof(NPCMovement))]
[CanEditMultipleObjects]
public class NPCMovementEditor : Editor
{
    public override void OnInspectorGUI()
    {
        NPCMovement npcMovement = (NPCMovement)target;

        npcMovement.speed = EditorGUILayout.FloatField("Speed", npcMovement.speed);

        if (npcMovement.waypointRoutes == null)
        {
            npcMovement.waypointRoutes = new List<WaypointRoute>();
        }

        for (int i = 0; i < npcMovement.waypointRoutes.Count; i++)
        {
            EditorGUILayout.LabelField("Route " + (i + 1));
            npcMovement.waypointRoutes[i].waypointParent = (Transform)EditorGUILayout.ObjectField("Waypoint Parent", npcMovement.waypointRoutes[i].waypointParent, typeof(Transform), true);
            npcMovement.waypointRoutes[i].delay = EditorGUILayout.FloatField("Delay", npcMovement.waypointRoutes[i].delay);
            npcMovement.waypointRoutes[i].nextWaypointParent1 = (Transform)EditorGUILayout.ObjectField("Next Route 1", npcMovement.waypointRoutes[i].nextWaypointParent1, typeof(Transform), true);
            npcMovement.waypointRoutes[i].nextWaypointParent2 = (Transform)EditorGUILayout.ObjectField("Next Route 2", npcMovement.waypointRoutes[i].nextWaypointParent2, typeof(Transform), true);
            npcMovement.waypointRoutes[i].nextWaypointParent3 = (Transform)EditorGUILayout.ObjectField("Next Route 3", npcMovement.waypointRoutes[i].nextWaypointParent3, typeof(Transform), true);

            if (GUILayout.Button("Remove Route"))
            {
                npcMovement.waypointRoutes.RemoveAt(i);
                break; // Break out of the loop to avoid an ArgumentOutOfRangeException
            }
        }

        if (GUILayout.Button("Add Route"))
        {
            npcMovement.waypointRoutes.Add(new WaypointRoute());
        }
    }

    protected virtual void OnSceneGUI()
    {
        NPCMovement npcMovement = (NPCMovement)target;

        if (npcMovement.waypointRoutes != null)
        {
            for (int j = 0; j < npcMovement.waypointRoutes.Count; j++)
            {
                Transform waypointParent = npcMovement.waypointRoutes[j].waypointParent;
                if (waypointParent != null)
                {
                    for (int i = 0; i < waypointParent.childCount; i++)
                    {
                        EditorGUI.BeginChangeCheck();
                        Vector3 newTargetPosition = Handles.PositionHandle(waypointParent.GetChild(i).position, Quaternion.identity);
                        if (EditorGUI.EndChangeCheck())
                        {
                            Undo.RecordObject(npcMovement, "Changed waypoint position");
                            waypointParent.GetChild(i).position = newTargetPosition;
                        }
                    }
                }
            }

            // If the user clicks in the scene view, add a new waypoint at that position
            if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
            {
                Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Undo.RecordObject(npcMovement, "Added waypoint");
                    GameObject waypoint = new GameObject("Waypoint");
                    waypoint.transform.position = hit.point;
                    if (npcMovement.waypointRoutes.Count > 0)
                    {
                        waypoint.transform.parent = npcMovement.waypointRoutes[npcMovement.waypointRoutes.Count - 1].waypointParent;
                    }
                }
            }
        }
    }
}
#endif