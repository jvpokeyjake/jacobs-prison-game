using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChatInputHandler : MonoBehaviour
{
    [SerializeField] private TMP_InputField chatInput;
    public static string LastTypedMessage { get; set; }
    public static bool IsTyping { get; private set; }
    private const int MaxCharacters = 96;
    
    private void Start()
    {
        chatInput.onSubmit.AddListener(OnMessageSubmit);
        chatInput.characterLimit = MaxCharacters;
        
        // Add listeners for when the input field is selected/deselected
        chatInput.onSelect.AddListener(_ => IsTyping = true);
        chatInput.onDeselect.AddListener(_ => IsTyping = false);
    }

    private void OnMessageSubmit(string message)
    {
        if (string.IsNullOrWhiteSpace(message)) return;
        
        LastTypedMessage = message;
        chatInput.text = "";
        
        // Deselect the input field after submitting
        chatInput.DeactivateInputField();
        
        // Remove focus from the input field
        EventSystem.current.SetSelectedGameObject(null);
    }

    private void OnDestroy()
    {
        chatInput.onSubmit.RemoveListener(OnMessageSubmit);
        chatInput.onSelect.RemoveAllListeners();
        chatInput.onDeselect.RemoveAllListeners();
    }
}