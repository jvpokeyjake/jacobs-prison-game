using TMPro;
using Unity.Netcode;
using UnityEngine;

public class PlayerChatDisplay : NetworkBehaviour
{
    [SerializeField] private GameObject speechBubble;
    [SerializeField] private TextMeshProUGUI chatText;
    
    [SerializeField] private float messageDisplayTime = 5f;
    private float currentDisplayTimer;

    public override void OnNetworkSpawn()
    {
        if (speechBubble != null)
        {
            speechBubble.SetActive(false);
        }
    }

    private void Update()
    {
        if (!IsOwner) return;

        // Check for new message
        if (!string.IsNullOrEmpty(ChatInputHandler.LastTypedMessage))
        {
            UpdateMessageServerRpc(ChatInputHandler.LastTypedMessage);
            // Update local display immediately for responsiveness
            UpdateLocalDisplay(ChatInputHandler.LastTypedMessage);
            ChatInputHandler.LastTypedMessage = null;
        }

        // Handle message timer
        if (currentDisplayTimer > 0)
        {
            currentDisplayTimer -= Time.deltaTime;
            if (currentDisplayTimer <= 0)
            {
                if (IsOwner)
                {
                    ClearMessageServerRpc();
                }
                HideMessage();
            }
        }
    }

    [ServerRpc]
    private void UpdateMessageServerRpc(string message)
    {
        UpdateMessageClientRpc(message);
    }

    [ClientRpc]
    private void UpdateMessageClientRpc(string message)
    {
        if (IsOwner) return; // Owner already updated locally
        UpdateLocalDisplay(message);
    }

    [ServerRpc]
    private void ClearMessageServerRpc()
    {
        ClearMessageClientRpc();
    }

    [ClientRpc]
    private void ClearMessageClientRpc()
    {
        HideMessage();
    }

    private void UpdateLocalDisplay(string message)
    {
        if (chatText == null) return;
        
        chatText.text = message;
        if (speechBubble != null)
        {
            speechBubble.SetActive(true);
        }
        currentDisplayTimer = messageDisplayTime;
    }

    private void HideMessage()
    {
        if (speechBubble != null)
        {
            speechBubble.SetActive(false);
        }
        if (chatText != null)
        {
            chatText.text = "";
        }
        currentDisplayTimer = 0;
    }

    public override void OnNetworkDespawn()
    {
        HideMessage();
    }
}