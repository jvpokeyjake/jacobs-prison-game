﻿using System;

namespace UnityEngine
{
    public class CustomIsometricRuleTile<T> : CustomIsometricRuleTile
    {
        public sealed override Type m_NeighborType { get { return typeof(T); } }
    }

    [Serializable]
    [CreateAssetMenu(fileName = "New Isometric Rule Tile", menuName = "Tiles/Isometric Rule Tile")]
    public class CustomIsometricRuleTile : RuleTile
    {
        // This has no differences with the RuleTile
    }
}
