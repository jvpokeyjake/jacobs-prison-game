﻿using System;
using UnityEditor;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace UnityEditor
{
    [CustomEditor(typeof(CustomIsometricRuleTile), true)]
    [CanEditMultipleObjects]
    internal class IsometricRuleTileEditor : RuleTileEditor
    {
        private static readonly int[, ] s_Arrows =
        {
            { 3, 0, 1 },
            { 6, 9, 2 },
            { 7, 8, 5 },
        };

        public override void RuleMatrixOnGUI(RuleTile tile, Rect rect, BoundsInt bounds, RuleTile.TilingRule tilingRule)
        {
            Handles.color = EditorGUIUtility.isProSkin ? new Color(1f, 1f, 1f, 0.2f) : new Color(0f, 0f, 0f, 0.2f);
            int index = 0;
            float w = rect.width / bounds.size.x;
            float h = rect.height / bounds.size.y;

            // Grid
            for (int y = 0; y <= bounds.size.y; y++)
            {
                float left = rect.xMin + (y * rect.width) / 6;
                float right = left + rect.width / 2;
                float bottom = rect.yMin + (y * rect.height) / 6;
                float top = bottom + rect.height / 2;
                Handles.DrawLine(new Vector3(left, top), new Vector3(right, bottom));
            }
            for (int x = 0; x <= bounds.size.x; x++)
            {
                float left = rect.xMin + (x * rect.width) / 6;
                float right = left + rect.width / 2;
                float top = rect.yMax - (x * rect.height) / 6;
                float bottom = top - rect.height / 2;
                Handles.DrawLine(new Vector3(left, bottom), new Vector3(right, top));
            }
            Handles.color = Color.white;

            // Icons
            for (int y = 0; y <= 2; y++)
            {
                for (int x = 0; x <= 2; x++)
                {
                    Rect r = new Rect(
                        rect.xMin + ((x + y) * rect.width) / 6, 
                        rect.yMin + ((2 - x + y) * rect.height) / 6, 
                        w - 1, h - 1);
                    if (x != 1 || y != 1)
                    {
                        Vector3Int position = new Vector3Int(x, y, 0); // Create a Vector3Int from the x and y
                        RuleOnGUI(r, position, tilingRule.m_Neighbors[index]); // Pass the Vector3Int to RuleOnGUI
                        Vector3Int pos = new Vector3Int(index, 0, 0); // Create a Vector3Int from the index
                        RuleNeighborUpdate(r, tilingRule, tilingRule.GetNeighbors(), pos); // Pass the Vector3Int and neighbors to RuleNeighborUpdate

                        index++;
                    }
                    else
                    {
                        RuleTransformOnGUI(r, tilingRule.m_RuleTransform);
                        RuleTransformUpdate(r, tilingRule); // No changes needed here
                    }
                }
            }
        }

        public override bool ContainsMousePosition(Rect rect)
        {
            var center = rect.center;
            var halfWidth = rect.width / 2;
            var halfHeight = rect.height / 2;
            var mouseFromCenter = Event.current.mousePosition - center;
            var xAbs = Mathf.Abs(Vector2.Dot(mouseFromCenter, Vector2.right));
            var yAbs = Mathf.Abs(Vector2.Dot(mouseFromCenter, Vector2.up));
            return (xAbs / halfWidth + yAbs / halfHeight) <= 1;
        }
    }
}
