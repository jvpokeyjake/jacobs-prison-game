﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPGM.Gameplay
{
    /// <summary>
    /// A simple camera follower class. It saves the offset from the
    ///  focus position when started, and preserves that offset when following the focus.
    /// </summary>
    public class CameraController : MonoBehaviour
    {
        public Transform focus;
        public float smoothTime = 2;

        Vector3 offset;

        void GetOffset()
        {
            offset = focus.position - transform.position;
        }

        void Update()
        {
            if(focus == null)
            {
                focus = GameObject.FindObjectOfType<OwnerOfCharacter>()?.characterController2D.transform;
                if(focus)
                {
                    GetOffset();
                }
                return;
            }
            transform.position = Vector3.Lerp(transform.position, focus.position - offset, Time.deltaTime * smoothTime);
        }
    }
}