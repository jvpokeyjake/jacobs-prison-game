using UnityEngine;
using Unity.Netcode;

namespace RPGM.Gameplay
{
    public class OwnerOfCharacter : NetworkBehaviour
    {
        public CharacterController2D characterController2D;
        
        public override void OnNetworkSpawn()
        {
            if (!IsOwner)
            {
                enabled = false;
                return;
            }
        }
    }
}