using RPGM.Core;
using RPGM.Gameplay;
using UnityEngine;

namespace RPGM.Gameplay
{
    /// <summary>
    /// Main class for implementing NPC game objects.
    /// </summary>
    public class NPCController : MonoBehaviour
    {
        public ConversationScript[] conversations;
        public float conversationTriggerDistance = 1.0f; // The distance within which the player can start a conversation
        private NPCMovement npcMovement;

        Quest activeQuest = null;

        Quest[] quests;

        GameModel model = Schedule.GetModel<GameModel>();

        void OnEnable()
        {
            quests = gameObject.GetComponentsInChildren<Quest>();
            npcMovement = GetComponent<NPCMovement>();
        }

        private void Update()
    {
        // Get the player's game object. You might need to replace this with your own method of accessing the player.
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for(int i = 0; i < players.Length; i++)
            {

            // Calculate the distance between the NPC and the player
            float distanceToPlayer = Vector3.Distance(transform.position, players[i].transform.position);

            if (ChatInputHandler.IsTyping) return;
            // If the player is within the trigger distance and the spacebar is pressed, and no conversation is in progress, start a conversation
            if (distanceToPlayer <= conversationTriggerDistance && Input.GetKeyDown(KeyCode.Space) && !npcMovement.isInConversation.Value)
            {
                var c = GetConversation();
                if (c != null)
                {
                    var ev = Schedule.Add<Events.ShowConversation>();
                    ev.conversation = c;
                    ev.npc = this;
                    ev.gameObject = gameObject;
                    ev.conversationItemKey = "";
                }
            }
        }
    }

        public void CompleteQuest(Quest q)
        {
            if (activeQuest != q) throw new System.Exception("Completed quest is not the active quest.");
            foreach (var i in activeQuest.requiredItems)
            {
                model.RemoveInventoryItem(i.item, i.count);
            }
            activeQuest.RewardItemsToPlayer();
            activeQuest.OnFinishQuest();
            activeQuest = null;
        }

        public void StartQuest(Quest q)
        {
            if (activeQuest != null) throw new System.Exception("Only one quest should be active.");
            activeQuest = q;
        }

        ConversationScript GetConversation()
        {
            if (activeQuest == null)
                return conversations[0];
            foreach (var q in quests)
            {
                if (q == activeQuest)
                {
                    if (q.IsQuestComplete())
                    {
                        CompleteQuest(q);
                        return q.questCompletedConversation;
                    }
                    return q.questInProgressConversation;
                }
            }
            return null;
        }
    }
}