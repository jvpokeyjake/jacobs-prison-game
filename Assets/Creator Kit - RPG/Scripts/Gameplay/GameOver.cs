using UnityEngine;
using TMPro;
using Unity.Netcode;

public class GameOver : NetworkBehaviour
{
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private TextMeshProUGUI resultText;
    
    public void CheckIfGameIsOver()
    {
        GameObject[] npcs = GameObject.FindGameObjectsWithTag("NPC");
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] allParticipants = new GameObject[npcs.Length + players.Length];
        
        npcs.CopyTo(allParticipants, 0);
        players.CopyTo(allParticipants, npcs.Length);

        //Check if there is 1 or less non-imposters left
        int nonImpostersLeft = 0;
        for(int i = 0; i < allParticipants.Length; i++)
        {
            if(allParticipants[i].CompareTag("Player"))
            {
                PlayerSymbol playerSymbol = allParticipants[i].GetComponent<PlayerSymbol>();
                if(!playerSymbol.isImposter.Value)
                {
                    PlayerDeath playerDeath = allParticipants[i].GetComponent<PlayerDeath>();
                    if(!playerDeath.isDead)
                    {
                        nonImpostersLeft++;
                    }
                }
            }
            else if(allParticipants[i].CompareTag("NPC"))
            {
                NPCSymbol npcSymbol = allParticipants[i].GetComponent<NPCSymbol>();
                if(!npcSymbol.isImposter.Value)
                {
                    NPCDeath npcDeath = allParticipants[i].GetComponent<NPCDeath>();
                    if(!npcDeath.isDead)
                    {
                        nonImpostersLeft++;
                    }
                }
            }
        }

        //Check if there is 0 imposters left
        int impostersLeft = 0;
        for(int i = 0; i < allParticipants.Length; i++)
        {
            if(allParticipants[i].CompareTag("Player"))
            {
                PlayerSymbol playerSymbol = allParticipants[i].GetComponent<PlayerSymbol>();
                if(playerSymbol.isImposter.Value)
                {
                    PlayerDeath playerDeath = allParticipants[i].GetComponent<PlayerDeath>();
                    if(!playerDeath.isDead)
                    {
                        impostersLeft++;
                    }
                }
            }
            else if(allParticipants[i].CompareTag("NPC"))
            {
                NPCSymbol npcSymbol = allParticipants[i].GetComponent<NPCSymbol>();
                if(npcSymbol.isImposter.Value)
                {
                    NPCDeath npcDeath = allParticipants[i].GetComponent<NPCDeath>();
                    if(!npcDeath.isDead)
                    {
                        impostersLeft++;
                    }
                }
            }
        }
        UnityEngine.Debug.Log("Imposters left: " + impostersLeft);
        UnityEngine.Debug.Log("Non-imposters left: " + nonImpostersLeft);
        if(impostersLeft == 0)
        {
            if(nonImpostersLeft == 0)
            {
                resultText.text = "Everyone is dead!";
            }
            else
            {
                resultText.text = "The Imposter has been defeated.";
            }
            gameOverUI.SetActive(true);
        }
        else if(nonImpostersLeft <= 1)
        {
            resultText.text = "The Imposter has won.";
            gameOverUI.SetActive(true);
        }
    }
}