using RPGM.Core;
using RPGM.Gameplay;
using RPGM.UI;
using UnityEngine;

namespace RPGM.Events
{
    public class ConversationTrigger : MonoBehaviour
    {
        public ShowConversation currentConversation;
        private bool spaceBarPressed = false;
        private float lastTriggerTime = 0f;
        private const float triggerCooldown = 0.5f; // Cooldown in seconds

        void OnEnable()
        {
            spaceBarPressed = false;
            lastTriggerTime = 0f;
        }

        void Update()
        {
            // If typing in chat, disable conversation triggers
            if (ChatInputHandler.IsTyping) return;

            // Add cooldown check to prevent rapid triggering
            if (Time.time - lastTriggerTime < triggerCooldown) return;

            if (!spaceBarPressed && Input.GetKeyDown(KeyCode.Space) && 
                !ReferenceEquals(currentConversation.spaceBarOption, null) && 
                currentConversation.spaceBarOption.text == "")
            {
                spaceBarPressed = true;
                lastTriggerTime = Time.time;

                var next = currentConversation.spaceBarOption.targetId;

                //Make sure it actually exists!
                if (currentConversation.conversation.ContainsKey(next))
                {
                    //find the conversation piece object and setup a new event with correct parameters.
                    var c = currentConversation.conversation.Get(next);
                    var ev = Schedule.Add<ShowConversation>(0.25f);
                    ev.conversation = currentConversation.conversation;
                    ev.gameObject = currentConversation.gameObject;
                    ev.conversationItemKey = next;
                    
                    // Preserve references from the current conversation
                    if (currentConversation.networkPlayer != null)
                        ev.networkPlayer = currentConversation.networkPlayer;
                    if (currentConversation.npc != null)
                        ev.npc = currentConversation.npc;

                    // Disable this script
                    this.enabled = false;
                }
                else
                {
                    Debug.LogError($"No conversation with ID:{next}");
                    // Reset conversation state
                    ShowConversation.ResetConversationState();
                }
            }
        }
        
        void OnDisable()
        {
            // Clean up when disabled
            spaceBarPressed = false;
        }
    }
}