using UnityEngine;
using TMPro;
using System.Collections;
using System.Linq;
using Unity.Netcode;
using LobbyRelaySample;

public class AssignImposter : NetworkBehaviour
{
    private GameManager gameManager;
    [SerializeField] private TextMeshPro imposterText;

    void Awake()
    {
        StartCoroutine(WaitUntilParticipantsHaveArrived(5));
    }

    [ServerRpc]
    public void AssignTheImposterServerRpc()
    {
        var npcs = GameObject.FindGameObjectsWithTag("NPC").ToList();
        var players = GameObject.FindGameObjectsWithTag("Player").ToList();
        var allParticipants = npcs.Concat(players).ToList();

        Random.InitState(System.DateTime.Now.Millisecond);
        int imposterIndex = Random.Range(0, allParticipants.Count);

        for (int i = 0; i < allParticipants.Count; i++)
        {
            bool isImposter = (i == imposterIndex);
            if (allParticipants[i].CompareTag("NPC"))
            {
                NPCSymbol npcSymbol = allParticipants[i].GetComponent<NPCSymbol>();
                npcSymbol.isImposter.Value = isImposter;
            }
            else if (allParticipants[i].CompareTag("Player"))
            {
                PlayerSymbol playerSymbol = allParticipants[i].GetComponent<PlayerSymbol>();
                playerSymbol.isImposter.Value = isImposter;
                playerSymbol.UpdateImposterText();
            }
        }

        UpdateClientsImposterStatusClientRpc(imposterIndex);
    }

    [ClientRpc]
    void UpdateClientsImposterStatusClientRpc(int imposterIndex)
    {
        var allParticipants = GameObject.FindGameObjectsWithTag("NPC")
            .Concat(GameObject.FindGameObjectsWithTag("Player")).ToList();

        for (int i = 0; i < allParticipants.Count; i++)
        {
            bool isImposter = (i == imposterIndex);
            if (allParticipants[i].CompareTag("Player"))
            {
                PlayerSymbol playerSymbol = allParticipants[i].GetComponent<PlayerSymbol>();
                // Remove the NetworkVariable assignment that was causing the error
                // playerSymbol.isImposter.Value = isImposter;
                playerSymbol.UpdateImposterText();
            }
            if (allParticipants[i].CompareTag("NPC"))
            {
                NPCSymbol npcSymbol = allParticipants[i].GetComponent<NPCSymbol>();
                // Remove the NetworkVariable assignment that was causing the error
                // npcSymbol.isImposter.Value = isImposter;
            }
        }
    }

    IEnumerator WaitUntilParticipantsHaveArrived(float delay)
    {
        yield return new WaitForSeconds(delay);
        gameManager = FindObjectOfType<GameManager>();
        if (gameManager.m_LocalUser.IsHost.Value)
        {
            AssignTheImposterServerRpc();
        }
        else
        {
            UnityEngine.Debug.LogError("Not the host");
        }
    }
}