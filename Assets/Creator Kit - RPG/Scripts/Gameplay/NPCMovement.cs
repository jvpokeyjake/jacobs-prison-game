using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Netcode;
using RPGM.Events;

[System.Serializable]
public class WaypointRoute
{
    public Transform waypointParent;
    public float delay;
    public Transform nextWaypointParent1;
    public Transform nextWaypointParent2;
    public Transform nextWaypointParent3;
}

public class NPCMovement : NetworkBehaviour
{
    public List<WaypointRoute> waypointRoutes;
    public float speed = 2.0f;
    private WaypointRoute currentRoute;
    private Animator animator;
    private Vector3 direction;
    public NetworkVariable<bool> isInConversation = new NetworkVariable<bool>(false);
    public NetworkVariable<bool> flipX = new NetworkVariable<bool>(false);

    private void Start()
    {
        currentRoute = waypointRoutes[0];
        animator = GetComponent<Animator>();
        StartCoroutine(MoveToWaypoints());
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        GetComponent<SpriteRenderer>().flipX = flipX.Value;
    }

    private void FixedUpdate()
    {
        if (isInConversation.Value)
        {
            // Stop the NPC's movement
            direction = Vector3.zero;
            animator.SetInteger("WalkX", 0);
            animator.SetInteger("WalkY", 0);
        }
        else if (IsHost)
        {
            if (direction != Vector3.zero)
            {
                int walkX = RoundToDirection(direction.x);
                int walkY = RoundToDirection(direction.y);

                // If both walkX and walkY are non-zero, keep the one closer to its original value
                if (walkX != 0 && walkY != 0)
                {
                    bool newFlipX = walkX > 0;
                    if (flipX.Value != newFlipX)
                    {
                        flipX.Value = newFlipX;
                        UpdateFlipXClientRpc(newFlipX);
                    }

                    if (Mathf.Abs(direction.x - walkX) < Mathf.Abs(direction.y - walkY))
                    {
                        walkY = 0;
                    }
                    else
                    {
                        walkX = 0;
                    }
                }

                animator.SetInteger("WalkX", walkX);
                animator.SetInteger("WalkY", -walkY);
            }
        }
    }

    [ClientRpc]
    private void UpdateFlipXClientRpc(bool newFlipX)
    {
        GetComponent<SpriteRenderer>().flipX = newFlipX;
    }

    private IEnumerator MoveToWaypoints()
    {
        while (true)
        {
            Transform waypointParent = currentRoute.waypointParent;
            int waypointCount = waypointParent.childCount;
            int waypointIndex = 0;
            int step = 1;

            while (waypointIndex >= 0 && waypointIndex < waypointCount)
            {
                Vector3 targetPosition = waypointParent.GetChild(waypointIndex).position;
                while (transform.position != targetPosition && !isInConversation.Value)
                {
                    direction = (targetPosition - transform.position).normalized;
                    transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed * Time.deltaTime);
                    yield return null;
                }

                direction = Vector3.zero;
                animator.SetInteger("WalkX", 0);
                animator.SetInteger("WalkY", 0);

                waypointIndex += step;
            }

            yield return new WaitForSeconds(currentRoute.delay);

            List<Transform> nextRoutes = new List<Transform>();
            if (currentRoute.nextWaypointParent1 != null) nextRoutes.Add(currentRoute.nextWaypointParent1);
            if (currentRoute.nextWaypointParent2 != null) nextRoutes.Add(currentRoute.nextWaypointParent2);
            if (currentRoute.nextWaypointParent3 != null) nextRoutes.Add(currentRoute.nextWaypointParent3);

            if (nextRoutes.Count > 0)
            {
                Random.InitState(System.DateTime.Now.Millisecond);
                int index = Random.Range(0, nextRoutes.Count);
                Transform nextRoute = nextRoutes[index];
                currentRoute = waypointRoutes.Find(route => route.waypointParent == nextRoute);
            }
        }
    }

    public void AddNewRoute(WaypointRoute route)
    {
        waypointRoutes.Add(route);
    }

    private int RoundToDirection(float value)
    {
        if (value > 0f)
        {
            return 1;
        }
        else if (value < 0f)
        {
            return -1;
        }
        else
        {
            return 0;
        }
    }
    
    public IEnumerator EndConversationWithDelay()
    {
        // Wait for a short duration
        yield return new WaitForSeconds(0.3f);

        bool spacePressed = false;
        
        // Loop until isInConversation is false or space is pressed
        while (isInConversation.Value && !spacePressed)
        {
            // Check if the spacebar is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                spacePressed = true;
                // Request the server to update the isInConversation variable
                UpdateIsInConversationServerRpc(false);
                
                // Reset the conversation state
                ShowConversation.ResetConversationState();
            }

            // Yield control back to Unity for this frame
            yield return null;
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void UpdateIsInConversationServerRpc(bool value)
    {
        isInConversation.Value = value;
    }
}