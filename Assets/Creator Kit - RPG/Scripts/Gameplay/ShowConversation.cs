﻿using System.Collections;
using System.Collections.Generic;
using RPGM.Core;
using RPGM.Gameplay;
using RPGM.UI;
using UnityEngine;

namespace RPGM.Events
{
    /// <summary>
    /// This event will start a conversation with an NPC using a conversation script.
    /// </summary>
    /// <typeparam name="ShowConversation"></typeparam>
    public class ShowConversation : Event<ShowConversation>
    {
        // Static flag to track if any conversation is currently active
        private static bool isAnyConversationActive = false;
        
        public NPCController npc;
        public NetworkPlayerController networkPlayer;
        public GameObject gameObject;
        public ConversationScript conversation;
        public string conversationItemKey;
        public ConversationOption spaceBarOption;
        
        // Track if this specific conversation instance is processing input
        private bool isProcessingInput = false;
        private Coroutine activeCoroutine = null;

        public override void Execute()
        {
            // Prevent multiple conversations from starting simultaneously
            if (isAnyConversationActive && string.IsNullOrEmpty(conversationItemKey))
            {
                Debug.Log("Another conversation is already active. Ignoring new conversation start.");
                return;
            }
            
            // Mark conversation as active
            isAnyConversationActive = true;
            
            NPCMovement npcMovement = null;
            if(npc != null)
            {
                npcMovement = gameObject.GetComponent<NPCMovement>();
                npcMovement.UpdateIsInConversationServerRpc(true);
            }

            ConversationPiece ci;
            //default to first conversation item if no key is specified, else find the right conversation item.
            if (string.IsNullOrEmpty(conversationItemKey))
                ci = conversation.items[0];
            else
                ci = conversation.Get(conversationItemKey);


            //if this item contains an unstarted quest, schedule a start quest event for the quest.
            if (ci.quest != null)
            {
                if (!ci.quest.isStarted)
                {
                    var ev = Schedule.Add<StartQuest>(1);
                    ev.quest = ci.quest;
                    if(npc)
                        ev.npc = npc;
                }
                if (ci.quest.isFinished && ci.quest.questCompletedConversation != null)
                {
                    ci = ci.quest.questCompletedConversation.items[0];
                }
            }

            if (ci.functionRunner != null)
            {
                ci.functionRunner.Run();
            }

            //calculate a position above the player's sprite.
            var position = gameObject.transform.position;
            var sr = gameObject.GetComponent<SpriteRenderer>();
            if (sr != null)
            {
                position += new Vector3(0, 2 * sr.size.y + (ci.options.Count == 0 ? 0.1f : 0.2f), 0);
            }

            //show the dialog
            model.dialog.Show(position, ci.text);
            var animator = gameObject.GetComponent<Animator>();
            if (animator != null)
            {
                animator.SetBool("Talk", true);
                var ev = Schedule.Add<StopTalking>(2);
                ev.animator = animator;
            }

            if (ci.audio != null)
            {
                UserInterfaceAudio.PlayClip(ci.audio);
            }

            //speak some gibberish at two speech syllables per word.
            UserInterfaceAudio.Speak(gameObject.GetInstanceID(), ci.text.Split(' ').Length * 2, 1);

            //if this conversation item has an id, register it in the model.
            if (!string.IsNullOrEmpty(ci.id))
                model.RegisterConversation(gameObject, ci.id);

            //setup conversation choices, if any.
            if (ci.options.Count == 0)
            {
                if(npc)
                {
                    // Cancel any existing coroutine first
                    if (activeCoroutine != null)
                    {
                        npcMovement.StopCoroutine(activeCoroutine);
                    }
                    activeCoroutine = npcMovement.StartCoroutine(npcMovement.EndConversationWithDelay());
                }
                else if(networkPlayer)
                {
                    MonoBehaviour mb = gameObject.GetComponent<MonoBehaviour>();
                    // Cancel any existing coroutine first
                    if (activeCoroutine != null && mb != null)
                    {
                        mb.StopCoroutine(activeCoroutine);
                    }
                    
                    if (mb != null)
                    {
                        isProcessingInput = true;
                        activeCoroutine = mb.StartCoroutine(EndNetworkPlayerConversationWithDelay());
                    }
                }
            }
            else
            {
                //Create option buttons below the dialog.
                for (var i = 0; i < ci.options.Count; i++)
                {
                    if (ci.options[i].text != "")
                    {
                        model.dialog.SetButton(i, ci.options[i].text);
                    }
                    else
                    {
                        spaceBarOption = ci.options[i]; // Store the buttonless option
                        TriggerNewScript(spaceBarOption);
                    }
                }

                //if user pickes this option, schedule an event to show the new option.
                model.dialog.onButton += (index) =>
                {
                    // Check if index is a valid index for ci.options
                    if (index >= 0 && index < ci.options.Count)
                    {
                        //hide the old text, so we can display the new.
                        model.dialog.Hide();

                        //This is the id of the next conversation piece.
                        var next = ci.options[index].targetId;

                        //Make sure it actually exists!
                        if (conversation.ContainsKey(next))
                        {
                            //find the conversation piece object and setup a new event with correct parameters.
                            var c = conversation.Get(next);
                            var ev = Schedule.Add<ShowConversation>(0.25f);
                            ev.conversation = conversation;
                            ev.gameObject = gameObject;
                            ev.conversationItemKey = next;
                            
                            // Preserve the reference to the network player or NPC
                            if (networkPlayer != null)
                                ev.networkPlayer = networkPlayer;
                            if (npc != null)
                                ev.npc = npc;
                        }
                        else
                        {
                            Debug.LogError($"No conversation with ID:{next}");
                            
                            // End the conversation if there's no next conversation piece
                            if (networkPlayer != null)
                            {
                                networkPlayer.EndConversation();
                                isAnyConversationActive = false;
                                isProcessingInput = false;
                            }
                        }
                    }
                };
            }

            //if conversation has an icon associated, this will display it.
            model.dialog.SetIcon(ci.image);
        }

        private IEnumerator EndNetworkPlayerConversationWithDelay()
        {
            // Wait for a short duration to prevent immediate input processing
            yield return new WaitForSeconds(0.3f);

            // Loop until spacebar is pressed
            while (isProcessingInput)
            {
                // Check if the spacebar is pressed and we're still processing input
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    // Set flags to prevent further input processing
                    isProcessingInput = false;
                    isAnyConversationActive = false;
                    
                    // Hide dialog and end conversation
                    model.dialog.Hide();
                    if (networkPlayer != null)
                    {
                        networkPlayer.EndConversation();
                    }
                    break;
                }

                // Yield control back to Unity for this frame
                yield return null;
            }
            
            activeCoroutine = null;
        }

        private void TriggerNewScript(ConversationOption option)
        {
            // Get a reference to the ConversationTrigger script
            var conversationTriggers = gameObject.GetComponents<ConversationTrigger>();
            ConversationTrigger conversationTrigger = null;

            foreach (var trigger in conversationTriggers)
            {
                if (trigger.GetType() == typeof(ConversationTrigger))
                {
                    conversationTrigger = trigger;
                    break;
                }
            }

            // If the script is not attached to the game object, add it
            if (conversationTrigger == null)
            {
                conversationTrigger = gameObject.AddComponent<ConversationTrigger>();
            }

            // Set the current conversation in the ConversationTrigger script
            conversationTrigger.currentConversation = this;

            // Enable the ConversationTrigger script
            conversationTrigger.enabled = true;
        }
        
        // Public method to reset conversation state
        public static void ResetConversationState()
        {
            isAnyConversationActive = false;
        }
    }
}