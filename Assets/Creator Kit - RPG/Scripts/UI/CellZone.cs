using UnityEngine;
using RPGM.Gameplay;
using System.Collections;

public class CellZone : MonoBehaviour
{
    public GameObject chooseYourSymbolUI;
    private PlayerDeath playerDeath;
    private NPCDeath npcDeath;
    private CharacterController2D characterController2D;
    [HideInInspector] public bool starSelected;
    [HideInInspector] public bool moonSelected;
    [HideInInspector] public bool sunSelected;
    [HideInInspector] public bool cloudSelected;
    private bool playerInZone = false;
    private bool actionTriggered = false;
    private GameObject player;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            playerInZone = true;
            player = collision.gameObject;
        }
        else if(collision.gameObject.CompareTag("NPC") && RoundAndTime.time >= 21 * 60 && RoundAndTime.time <= 23 * 60)
        {
            NPCSymbol npcSymbol = collision.gameObject.GetComponent<NPCSymbol>();
            npcDeath = collision.gameObject.GetComponent<NPCDeath>();
            npcSymbol.GuessSymbol();
            npcDeath.npcGuessedRight = (npcSymbol.npcSymbol == npcSymbol.GuessSymbol());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            // Reset the camera scale if the player escapes the zone
            playerDeath = player.GetComponent<PlayerDeath>();
            Camera.main.transform.localScale = playerDeath.originalScale;
            chooseYourSymbolUI.SetActive(false);
            playerDeath = null;
            playerInZone = false;
            actionTriggered = false;
        }
        else if (collision.gameObject.CompareTag("NPC"))
        {
            if(npcDeath != null)
            npcDeath.npcGuessedRight = false;
        }
    }

    private IEnumerator SetIdleAnimation(GameObject player)
    {
        yield return new WaitForSeconds(2);
        Animator animator = player.GetComponent<Animator>();
        animator.SetInteger("WalkY", 1);
        yield return new WaitForSeconds(0.1f);
        animator.SetInteger("WalkX", 0);
        animator.SetInteger("WalkY", 0);
    }

    private void Update()
    {
        if (playerInZone && RoundAndTime.time >= 23 * 60 && !actionTriggered)
        {
            chooseYourSymbolUI.SetActive(true);
            playerDeath = player.GetComponent<PlayerDeath>();
            characterController2D = player.GetComponent<CharacterController2D>();
            characterController2D.enabled = false;
            StartCoroutine(SetIdleAnimation(player));
            actionTriggered = true; // Set the flag to true after the actions are triggered
        }

        if (playerDeath != null)
        {
            if(sunSelected && PlayerSymbol.playerSymbol == PlayerSymbol.Symbol.Sun)
            {
                playerDeath.playerGuessedRight = true;
            }
            else if(moonSelected && PlayerSymbol.playerSymbol == PlayerSymbol.Symbol.Moon)
            {
                playerDeath.playerGuessedRight = true;
            }
            else if(starSelected && PlayerSymbol.playerSymbol == PlayerSymbol.Symbol.Star)
            {
                playerDeath.playerGuessedRight = true;
            }
            else if(cloudSelected && PlayerSymbol.playerSymbol == PlayerSymbol.Symbol.Cloud)
            {
                playerDeath.playerGuessedRight = true;
            }
        }
        if((starSelected || moonSelected || sunSelected || cloudSelected) && characterController2D)
        {
            characterController2D.enabled = true;
            playerDeath = null;
            characterController2D = null;
        }
    }
}