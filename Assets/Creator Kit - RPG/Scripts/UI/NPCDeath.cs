using UnityEngine;
using RPGM.Gameplay;
using System.Collections;

public class NPCDeath : MonoBehaviour
{
    [HideInInspector] public bool npcGuessedRight = false;
    [HideInInspector] public bool isDead = false;
    private Animator animator;
    private bool resultGiven = false;
    [SerializeField] private NPCSymbol npcSymbol;

    private void Start()
    {
        // Get the animator component
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        // Check if the game time is after 23:00 and zooming has not been done yet
        if (RoundAndTime.time >= 00 * 60 && RoundAndTime.time <= 01 * 60 && !resultGiven)
        {
            StartCoroutine(ResolveNPCState());
            resultGiven = true;
        }

        if (RoundAndTime.time >= 01.1 * 60 && RoundAndTime.time <= 02 * 60)
        {
            npcGuessedRight = false;
            resultGiven = false;
        }
    }

    IEnumerator ResolveNPCState()
    {
        yield return new WaitForSeconds(2);

        if (npcGuessedRight)
        {
            Debug.Log("The NPC survived.");
            npcSymbol.AssignRandomSymbol();
        }
        else
        {
            Debug.Log("The NPC died.");
            GetComponent<NPCController>().enabled = false;
            GetComponent<ConversationScript>().enabled = false;
            NPCMovement npcMovement = GetComponent<NPCMovement>();
            npcMovement.speed = 0;
            npcMovement.enabled = false;
            animator.SetBool("IsDead", true);
            isDead = true;
        }
    }
}