using UnityEngine;
using RPGM.Gameplay;
using System.Collections;


public class PlayerDeath : MonoBehaviour
{
    [HideInInspector] public bool playerGuessedRight = false;
    [HideInInspector] public Vector3 originalScale;
    [HideInInspector] public bool isDead = false;
    private Camera mainCamera;
    private float zoomSpeed = 0.2f; // Adjust this value to change the speed of the zoom
    private bool isZooming = false;
    private bool zoomingDone = false;
    private Animator animator;
    private CharacterController2D characterController2D;
    public GameObject chooseYourSymbolUI;
    private GameOver gameOver;
    [SerializeField] private PlayerSymbol playerSymbol;

    private void Start()
    {
        // Get the main camera
        mainCamera = Camera.main;
        // Save the original scale
        originalScale = mainCamera.transform.localScale;
        // Get the animator component
        animator = GetComponent<Animator>();
        // Get the character controller component
        characterController2D = GetComponent<CharacterController2D>();
        // Get the ChooseYourSymbolUI GameObject
        chooseYourSymbolUI = GameObject.Find("Symbols");
        // Get the GameOver script
        gameOver = FindObjectOfType<GameOver>();
    }

    private void FixedUpdate()
    {
        // Check if the game time is after 23:00 and zooming has not been done yet
        if (RoundAndTime.time >= 00 * 60 && RoundAndTime.time <= 01 * 60 && !zoomingDone)
        {
            // Hide the UI
            chooseYourSymbolUI.SetActive(false);
            // Start zooming
            isZooming = true;
        }

        if (isZooming)
        {
            // Increase the scale of the camera to zoom in
            mainCamera.transform.localScale = Vector3.MoveTowards(mainCamera.transform.localScale, originalScale * 0.3f, zoomSpeed * Time.deltaTime);

            // Check if the zooming is finished
            if (Vector3.Distance(mainCamera.transform.localScale, originalScale * 0.3f) < 0.01f)
            {
                // Stop zooming
                isZooming = false;
                zoomingDone = true;
                // Log the result
                if (playerGuessedRight)
                {
                    Debug.Log("The player survived.");
                    playerSymbol.AssignRandomSymbol();
                }
                else
                {
                    Debug.Log("The player died.");
                    characterController2D.enabled = false;
                    animator.SetBool("IsDead", true);
                    isDead = true;
                }
                StartCoroutine(WaitAndGiveResult());
            }
        }

        if (RoundAndTime.time >= 01.1 * 60 && RoundAndTime.time <= 02 * 60)
        {
            playerGuessedRight = false;
            zoomingDone = false;
        }

        if (RoundAndTime.time >= 06.5 * 60 && RoundAndTime.time <= 07 * 60)
        {
            // Reset the camera scale for the next round
            mainCamera.transform.localScale = originalScale;
        }
    }
    
    IEnumerator WaitAndGiveResult()
    {
        yield return new WaitForSeconds(3);
        gameOver.CheckIfGameIsOver();
    }
}