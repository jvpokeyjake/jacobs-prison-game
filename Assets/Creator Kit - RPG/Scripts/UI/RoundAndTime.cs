using System.Collections;
using TMPro;
using UnityEngine;
using Unity.Netcode;

public class RoundAndTime : NetworkBehaviour
{
    public TextMeshPro roundAndTimeText;
    public TextMeshPro periodText;
    
    // Keep public static values for other scripts to reference
    public static int round = 1;
    public static float time = 6.5f * 60f; // Start at 06:30
    
    // Network variables for synchronization
    private NetworkVariable<int> networkRound = new NetworkVariable<int>(1);
    private NetworkVariable<float> networkTime = new NetworkVariable<float>(6.5f * 60f);
    
    private const float realSecondsPerGameDay = 12 * 60; // 10 minutes in real life
    private const float gameSecondsPerGameDay = 24 * 60; // 24 hours in game

    public override void OnNetworkSpawn()
    {
        // Subscribe to network variable changes
        networkRound.OnValueChanged += (int oldValue, int newValue) => round = newValue;
        networkTime.OnValueChanged += (float oldValue, float newValue) => time = newValue;

        if (IsServer)
        {
            StartCoroutine(UpdateTime());
        }
    }

    private IEnumerator UpdateTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            // Update network variables
            networkTime.Value += gameSecondsPerGameDay / realSecondsPerGameDay;

            if (networkTime.Value >= 24 * 60)
            {
                networkRound.Value++;
                networkTime.Value = 0;
            }

            // Update static values (server-side)
            time = networkTime.Value;
            round = networkRound.Value;

            UpdateDisplay();
        }
    }

    private void Update()
    {
        if (!IsServer)
        {
            UpdateDisplay();
        }
    }

    private void UpdateDisplay()
    {
        // Update the time TextMeshPro field
        int hours = Mathf.FloorToInt(networkTime.Value / 60);
        int minutes = Mathf.FloorToInt(networkTime.Value % 60);
        roundAndTimeText.text = $"Round {networkRound.Value} - {hours:D2}:{minutes:D2}";

        // Update the period TextMeshPro field
        if (hours >= 7 && hours < 8)
        {
            periodText.text = "Breakfast";
        }
        else if (hours >= 12 && hours < 13)
        {
            periodText.text = "Lunch";
        }
        else if (hours >= 17 && hours < 18)
        {
            periodText.text = "Dinner";
        }
        else if (hours >= 21 && hours < 23)
        {
            periodText.text = "Return to Cell";
        }
        else
        {
            periodText.text = "";
        }
    }
}